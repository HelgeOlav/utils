package utils

import "testing"

func TestStringArray_IsInList(t *testing.T) {
	type args struct {
		input interface{}
	}
	stringX := "x"
	sar := StringArray{stringX, "y", "3"}
	tests := []struct {
		name string
		s    StringArray
		args args
		want bool
	}{
		{"Test for x", sar, args{stringX}, true},
		{"Test for *x", sar, args{&stringX}, true},
		{"Test for searcher", sar, args{"searcher"}, false},
		{"Test for 3", sar, args{3}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.s.IsInList(tt.args.input); got != tt.want {
				t.Errorf("IsInList() = %v, want %v", got, tt.want)
			}
		})
	}
}
