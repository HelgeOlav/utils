package utils

import (
	"errors"
	"fmt"
	"strings"
	"sync"
)

// Errors let you store many errors, and return them together.
type Errors struct {
	lock sync.Mutex
	errs []error
}

// Error returns the current error as a string.
func (e *Errors) Error() string {
	e.lock.Lock()
	defer e.lock.Unlock()
	switch len(e.errs) {
	case 0:
		return "no error"
	case 1:
		return e.errs[0].Error()
	default:
		var sb strings.Builder
		_, _ = sb.WriteString(fmt.Sprintf("%d errors: ", len(e.errs)))
		for x := range e.errs {
			_, _ = sb.WriteString("\n * " + e.errs[x].Error())
		}
		return sb.String()
	}
}

// Add lets you add one or more errors into the list
func (e *Errors) Add(errs ...error) {
	for x := range errs {
		e.add(errs[x])
	}
}

// add adds a new error into the list
func (e *Errors) add(err error) {
	if err == nil {
		return
	}
	if t, ok := err.(*Errors); ok {
		e.lock.Lock()
		t.lock.Lock()
		e.errs = append(e.errs, t.errs...)
		t.lock.Unlock()
		e.lock.Unlock()
	} else {
		e.lock.Lock()
		e.errs = append(e.errs, err)
		e.lock.Unlock()
	}
}

// ErrOrNil returns either nil if there are no errors, or the error.
// If it is only one error then the actual error is returned.
func (e *Errors) ErrOrNil() error {
	if e == nil {
		return nil
	}
	e.lock.Lock()
	defer e.lock.Unlock()
	switch len(e.errs) {
	case 0:
		return nil
	case 1:
		return e.errs[0]
	default:
		return e
	}
}

// Is reports whether any error in err's tree matches target.
func (e *Errors) Is(target error) bool {
	e.lock.Lock()
	defer e.lock.Unlock()
	for x := range e.errs {
		if errors.Is(e.errs[x], target) {
			return true
		}
	}
	return false
}

// As finds the first error in err's tree that matches target, and if one is found, sets target to that error value and returns true. Otherwise, it returns false.
func (e *Errors) As(target any) bool {
	e.lock.Lock()
	defer e.lock.Unlock()
	for x := range e.errs {
		if errors.As(e.errs[x], target) {
			return true
		}
	}
	return false
}

// Reset removes all errors from the list
func (e *Errors) Reset() {
	if e != nil {
		e.lock.Lock()
		e.errs = nil
		e.lock.Unlock()
	}
}

// Len returns the number of errors stored
func (e *Errors) Len() int {
	if e == nil {
		return 0
	}
	e.lock.Lock()
	defer e.lock.Unlock()
	return len(e.errs)
}

// All returns all errors so they can be inspected by hand.
func (e *Errors) All() []error {
	if e == nil {
		return nil
	}
	e.lock.Lock()
	defer e.lock.Unlock()
	return e.errs

}
