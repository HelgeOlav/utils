package utils

import (
	"testing"
)

func TestReadLines(t *testing.T) {
	lines, err := ReadLines("readfile.go", false)
	if err != nil {
		t.Error(err.Error())
	}
	if len(lines) < 10 {
		t.Error("Expected more lines when reading from file; got", len(lines))
	} else {
		if lines[0][:7] != "package" {
			t.Error("Expected first line to be package but got:", lines[0])
		}
	}
}
