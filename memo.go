package utils

// memo is inspired from React useMemo

// Memo lets you cache any result as long as you send in the same T every time.
type Memo[V any, T comparable] struct {
	lastVal T         // last value if T
	result  V         // our result
	updater func(T) V // method to call to update V
}

// NewMemo returns a new Memo object with a computed result for T.
func NewMemo[V any, T comparable](callback func(T) V, initialValue T) Memo[V, T] {
	return Memo[V, T]{
		lastVal: initialValue,
		result:  callback(initialValue),
		updater: callback,
	}
}

// Get returns V if T is the same as before, otherwise it will call your code to get a new value.
func (m *Memo[V, T]) Get(val T) V {
	if val != m.lastVal {
		m.lastVal = val
		m.result = m.updater(val)
	}
	return m.result
}
