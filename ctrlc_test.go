package utils

import (
	"testing"
)

func TestCtrlCchannel(t *testing.T) {
	for x := 0; x < 5; x++ {
		go func() {
			r := CtrlCchannel()
			if r == nil {
				t.Error("CtrlCchannel() returned nil")
			}
		}()
	}
}
