package utils

import (
	"context"
	"log/slog"
)

/*
This logger will accept and discard everything that is received. Use this way:

logger := slog.New(utils.DiscardSlog{})
*/

type DiscardSlog struct {
}

func (d DiscardSlog) Enabled(_ context.Context, _ slog.Level) bool {
	return false
}

func (d DiscardSlog) Handle(_ context.Context, _ slog.Record) error {
	return nil
}

func (d DiscardSlog) WithAttrs(_ []slog.Attr) slog.Handler {
	return DiscardSlog{}
}

func (d DiscardSlog) WithGroup(_ string) slog.Handler {
	return DiscardSlog{}
}
