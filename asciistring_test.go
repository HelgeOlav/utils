package utils

import "testing"

func Test_onlyAsciiText(t *testing.T) {
	type args struct {
		input string
	}
	tests := []struct {
		name       string
		args       args
		wantOutput string
	}{
		{name: "Test 1", args: args{input: "AZaz0123456789"}, wantOutput: "AZaz0123456789"},
		{name: "Test 2", args: args{input: "A 9"}, wantOutput: "A9"},
		{name: "Test 3", args: args{input: "øæå"}, wantOutput: ""},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotOutput := OnlyAsciiText(tt.args.input); gotOutput != tt.wantOutput {
				t.Errorf("onlyAsciiText() = %v, want %v", gotOutput, tt.wantOutput)
			}
		})
	}
}
