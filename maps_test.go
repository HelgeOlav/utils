package utils

import (
	"testing"
	"time"
)

func TestMapDeepCopy(t *testing.T) {
	// test on ints
	map1 := map[string]int{"val1": 1, "val2": 2, "val3": 3}
	copy1 := MapDeepCopy(map1)
	if len(copy1) != len(map1) {
		t.Error("wrong len for map copy")
	}
	// test strings
	map2 := map[string]string{"key1": "a key", "key2": "another key"}
	copy2 := MapDeepCopy(map2)
	if len(copy2) != len(map2) {
		t.Error("wrong len for map copy")
	}
	// test interface type
	map3 := map[int]interface{}{3: "hello", 2: nil, 1: time.Now()}
	copy3 := MapDeepCopy(map3)
	if len(copy3) != len(map3) {
		t.Error("wrong len for map copy")
	}
}
