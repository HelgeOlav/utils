package utils

import (
	"context"
	"encoding/json"
	"io"
	"os"
	"path"
)

// FS is an interface to access files in object storage.
// os.ErrNotExist is given when an object does not exist (checked with errors.Is().)
type FS interface {
	Put(ctx context.Context, objectName string, metadata map[string]string, r io.Reader) error // put an object to storage, caller has to close the Reader themselves
	Get(ctx context.Context, objectName string) (io.ReadCloser, map[string]string, error)      // get an object from storage
	Head(ctx context.Context, objectName string) (map[string]string, error)                    // get metadata from storage
	Delete(ctx context.Context, objectName string) error                                       // remove an object from storage
}

// NewLocalFS returns a FS that uses a directory on the local filesystem.
// This file system is for proof-of-concept only, as it is expected to use a real object storage as the backend.
// localFS can be escaped using relative paths (../../../etc)
func NewLocalFS(basePath string) FS {
	return &localFS{base: basePath, metaSuffix: ".metadata"}
}

// localFS is a local file-system based on FS
type localFS struct {
	base, metaSuffix string // base path
}

func (l *localFS) Put(_ context.Context, objectName string, metadata map[string]string, r io.Reader) error {
	fn := path.Join(l.base, objectName)
	md := fn + l.metaSuffix
	_ = os.Remove(md)
	f, err := os.Create(fn)
	if err != nil {
		return err
	}
	_, err = io.Copy(f, r)
	_ = f.Close()
	if err != nil {
		_ = os.Remove(fn)
		return err
	}
	if metadata != nil {
		bb, _ := json.Marshal(metadata)
		err = os.WriteFile(md, bb, os.ModePerm)
	}
	return err
}

func (l *localFS) Get(ctx context.Context, objectName string) (io.ReadCloser, map[string]string, error) {
	m, _ := l.Head(ctx, objectName)
	fn := path.Join(l.base, objectName)
	f, err := os.Open(fn)
	return f, m, err
}

func (l *localFS) Head(_ context.Context, objectName string) (map[string]string, error) {
	// first check if the object exists
	fn := path.Join(l.base, objectName)
	_, err := os.Stat(fn)
	if err != nil {
		return nil, os.ErrNotExist
	}
	// read the file, parse the content and return it
	m := make(map[string]string)
	fn = fn + l.metaSuffix
	fc, err := os.ReadFile(fn)
	if err != nil {
		return m, nil
	}
	err = json.Unmarshal(fc, &m)
	return m, err
}

func (l *localFS) Delete(_ context.Context, objectName string) error {
	fn := path.Join(l.base, objectName)
	_ = os.Remove(fn + l.metaSuffix)
	return os.Remove(fn)
}
