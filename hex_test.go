package utils

import (
	"fmt"
	"testing"
)

func TestToHex(t *testing.T) {
	// simple conversion
	test := []byte{1, 16}
	result := ToHex(test)
	fmt.Println(result)
	// nil
	result = ToHex(nil)
	if result != "" {
		t.Error("ToHex failed on nil - returned non-empty string")
	}
}
