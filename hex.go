package utils

import "encoding/hex"

// ToHex converts input stream to printable hex value
func ToHex(input []byte) string {
	if input == nil || len(input) == 0 {
		return ""
	}
	result := make([]byte, hex.EncodedLen(len(input)))
	hex.Encode(result, input)
	return string(result)
}
