# Utils

This package / repository contains small utilities that I have written and found useful for myself.

* OnlyAsciiText(string) string - returns only printable ASCII characters without spaces.
* ReadLines(string,bool) []string,error - reads the content of the file into a string array, each line as its own item. Optionally skips blank lines.
* ReadJsonFile - reads a JSON config from file
* ReadConfig - utility to help you in loading configuration files
* IsStringInString - helper to check if a string is in a slice/array
* Searcher - interface to search if something is present in something else
* DLock - a lightweight distributed lock that can be used either locally or via REST API
* Broadcast - get notified when a variable/object changes
* Cache - maintains a cached copy of an object that will be refreshed on access
* FS - an interface to access object storage of some kind
* Errors - a place to store errors if you don't want or have to check them until the end