package version

import (
	"fmt"
	"runtime"
	"strings"
)

// exported variables
var (
	NAME      string    // name of program
	VERSION   = "0.0.0" // version of the program
	BUILDTIME string    // build-time of the compiled program
	GITCOMMIT string    // commit signature
	DEVSUFFIX = "-dev"  // appended to the version without a GITCOMMIT
)

type Version struct {
	Name      string          `json:"name,omitempty"`      // name of program
	Version   string          `json:"version"`             // version of the program
	GoVersion string          `json:"goversion"`           // go compiler
	BuildTime string          `json:"buildtime,omitempty"` // built time
	GitCommit string          `json:"gitcommit,omitempty"` // commit hash
	Modules   []ModuleVersion `json:"modules,omitempty"`   // a list of registered modules
}

// ModuleVersion allows a module/plugin to register itself with a version number
type ModuleVersion struct {
	Name      string `json:"name,omitempty"`      // name of the module
	Version   string `json:"version"`             // version of the module
	GitCommit string `json:"gitcommit,omitempty"` // commit hash, if available
}

var modules []ModuleVersion // all registered modules

// AddModule registers a new module
func AddModule(m ModuleVersion) {
	modules = append(modules, m)
}

// LongString returns String() + (AllModules())
func (t Version) LongString() string {
	result := t.String()
	mods := t.AllModules()
	if len(mods) > 0 {
		result += " (" + mods + ")"
	}
	return result
}

// String returns a string of the Version struct
func (t Version) String() string {
	output := t.Name
	if output != "" {
		output += " "
	}
	output += t.Version
	if len(t.GitCommit) == 0 {
		output += DEVSUFFIX
	}
	if t.BuildTime != "" {
		output += fmt.Sprintf(" [%s]", t.BuildTime)
	}
	if t.GitCommit != "" {
		output += fmt.Sprintf(" (%s)", t.GitCommit)
	}
	return output
}

// Get returns Version
func Get() (ver Version) {
	return Version{
		Name:      NAME,
		Version:   VERSION,
		GoVersion: runtime.Version(),
		BuildTime: BUILDTIME,
		GitCommit: GITCOMMIT,
		Modules:   modules,
	}
}

// SimpleVersion returns a version string like progname/version[-dev]
func (v Version) SimpleVersion() string {
	if len(v.GitCommit) == 0 {
		return v.Name + "/" + v.Version + DEVSUFFIX
	}
	return v.Name + "/" + v.Version
}

// AllModules lists all modules with shortname/version
func (v Version) AllModules() string {
	if len(v.Modules) == 0 {
		return ""
	}
	var result []string
	for x := range v.Modules {
		var modname string
		if len(v.Modules[x].Name) == 0 {
			continue
		}
		pos := strings.LastIndex(v.Modules[x].Name, "/")
		if pos == -1 {
			modname = v.Modules[x].Name
		} else {
			modname = v.Modules[x].Name[pos+1:]
		}
		result = append(result, modname+"/"+v.Modules[x].Version)
	}
	return strings.Join(result, " ")
}
