package version

import (
	"fmt"
	"testing"
)

func TestGet(t *testing.T) {
	NAME = "test"
	VERSION = "1.1.1"
	BUILDTIME = "now"
	GITCOMMIT = "12345"
	fmt.Println("Tetser")
	v := Get()
	// check string
	result := v.String()
	const expString = "test 1.1.1 [now] (12345)"
	if result != expString {
		t.Errorf("String returned %s, expected %s\n", result, expString)
	}
	// check simpleversion
	result = v.SimpleVersion()
	const expSimpleVersion = "test/1.1.1"
	if result != expSimpleVersion {
		t.Errorf("SimpleVersion returned %s, expected %s\n", result, expSimpleVersion)
	}
	// check simpleversion without GITCOMMIT
	v.GitCommit = ""
	result = v.SimpleVersion()
	const expSimpleVersionDev = "test/1.1.1-dev"
	if result != expSimpleVersionDev {
		t.Errorf("SimpleVersion (with dev) returned %s, expected %s\n", result, expSimpleVersion)
	}
}

func TestVersion_AllModules(t *testing.T) {
	type fields struct {
		Name      string
		Version   string
		GoVersion string
		BuildTime string
		GitCommit string
		Modules   []ModuleVersion
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{"Test 1", fields{Modules: []ModuleVersion{{Name: "mod1", Version: "0.1"}, {Name: "mod2", Version: "0.2"}}}, "mod1/0.1 mod2/0.2"},
		{"Test 2", fields{Modules: []ModuleVersion{{Name: "mod1", Version: "0.1"}, {Name: "bitbucket.org/mod2", Version: "0.2"}}}, "mod1/0.1 mod2/0.2"},
		{"Test 3", fields{Modules: []ModuleVersion{{Name: "mod1", Version: "0.1"}, {Name: "bitbucket.org/HelgeOlav/mod2", Version: "0.2"}}}, "mod1/0.1 mod2/0.2"},
		{"Test 4", fields{Modules: []ModuleVersion{{Name: "mod1", Version: "0.1"}, {Name: "bitbucket.org/HelgeOlav/mod2/", Version: "0.2"}}}, "mod1/0.1 /0.2"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			v := Version{
				Name:      tt.fields.Name,
				Version:   tt.fields.Version,
				GoVersion: tt.fields.GoVersion,
				BuildTime: tt.fields.BuildTime,
				GitCommit: tt.fields.GitCommit,
				Modules:   tt.fields.Modules,
			}
			if got := v.AllModules(); got != tt.want {
				t.Errorf("AllModules() = %v, want %v", got, tt.want)
			}
		})
	}
}
