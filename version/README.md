# version

This is a simple version package to assist in returning a consistent version number for your program. It is inspired by https://github.com/tsaikd/KDGoLib own version package, but without all external dependencies.

This is a simple example on use:

```go
package main

import (
	"fmt"
	"bitbucket.org/HelgeOlav/utils/version"
)

func init() {
	version.VERSION = "1.0.1" // whatever you want
	version.NAME = "myprog" // the name of your program
}

func main() {
	fmt.Println(version.Get().LongString())
}
```

You can also add compile time constants on Linux/Mac using a compile-script like this:
```bash
githash="$(git rev-parse HEAD | cut -c1-8)"
buildtime="$(date +%Y-%m-%d)"

LDFLAGS="${LDFLAGS} -X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=${buildtime}"
LDFLAGS="${LDFLAGS} -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=${githash}"

go build -ldflags "${LDFLAGS}"
```

To compile on Windows you can use a script like this:
```
git rev-parse HEAD > hash.txt
date /t > date.txt
set /p GIT=<hash.txt
set /p STAMP=<date.txt

go build -ldflags="-X bitbucket.org/HelgeOlav/utils/version.BUILDTIME=%STAMP% -X bitbucket.org/HelgeOlav/utils/version.GITCOMMIT=%GIT%" .
```