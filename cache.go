package utils

import (
	"sync"
	"time"
)

// This caches one object and tries to retrieve a new object when the cache expires.
// The old data is returned as long as the new object has not been retrieved.
// The callback function returns the new value, true or false if a new value cannot be obtained.
// On a failed update a new update will be tried on next GetValue()

// Cache represents an object that can be cached
type Cache[T any] struct {
	value            *T               // the cached value if not nil
	lock             sync.RWMutex     // mutex to protect the value, updateInProgress and expire
	wait             *sync.Cond       // blocks access to the result if there is none, nil=no wait
	duration         time.Duration    // how long the cached entry should live
	expires          time.Time        // when the cached value expires
	fetch            func() (T, bool) // callback to get a new value - is only accepted if returns true
	updateInProgress bool             // true if an update is in progress
	updateStatus     int              // status on the initial update
	epoch            uint             // the current epoch, basically how many times we successfully have retrieved data from fetch
}

const (
	cacheInitialUpdateStatusNoDataNotTried  = 2 // state when we have not attempted initial fetch of data
	cacheInitialUpdateStatusNoDataHaveTried = 1 // state when fetch returned false
	cacheInitialUpdateStatusNormalOperation = 0 // state when fetch returned true
)

// NewCache creates a new cached object
func NewCache[T any](expire time.Duration, initialValue T, callback func() (T, bool)) *Cache[T] {
	c := Cache[T]{
		duration:     expire,
		value:        &initialValue,
		updateStatus: cacheInitialUpdateStatusNoDataNotTried,
		fetch:        callback,
	}
	return &c
}

// NewCacheWithoutInitialValue creates a new cached object where there is no initial value, and GetValue blocks until some data is available.
// In the case when the first fetch fails, a default value will be passed back instead.
func NewCacheWithoutInitialValue[T any](expire time.Duration, defaultValue T, callback func() (T, bool)) *Cache[T] {
	c := Cache[T]{
		duration:     expire,
		fetch:        callback,
		updateStatus: cacheInitialUpdateStatusNoDataNotTried,
		value:        &defaultValue,
		wait:         sync.NewCond(&sync.Mutex{}),
	}
	return &c
}

// GetValue returns the last value, and if the object has expired will try to retrieve a new object.
// If used without an initial value, this call will block until the first set of data is returned.
func (c *Cache[T]) GetValue() T {
	c.lock.RLock()
	defer c.lock.RUnlock()
	if time.Now().After(c.expires) {
		go c.Expire()
	}
	if c.wait != nil {
		if c.updateStatus == cacheInitialUpdateStatusNoDataNotTried {
			c.lock.RUnlock()
			c.wait.L.Lock()
			c.wait.Wait()
			c.lock.RLock()
		}
	}
	return *c.value
}

// Expire expires the object and also initiates retrieving new data
func (c *Cache[T]) Expire() {
	c.lock.Lock()
	if !c.updateInProgress {
		c.updateInProgress = true
		go c.updateObject()
	}
	c.lock.Unlock()
}

// updateObject is called when the cached object is to be updated - updateInProgress has to be set before calling this method.
// It will not lock until a new object is retrieved.
func (c *Cache[T]) updateObject() {
	newVal, ok := c.fetch()
	//log.Println("updateObject started")
	c.lock.Lock()
	c.updateInProgress = false
	if ok {
		c.value = &newVal
		c.expires = time.Now().Add(c.duration)
		c.updateStatus = cacheInitialUpdateStatusNormalOperation
		c.epoch++
	} else {
		c.updateStatus = cacheInitialUpdateStatusNoDataHaveTried
	}
	if c.wait != nil {
		c.wait.Broadcast()
		c.wait = nil
	}
	//log.Println("updateObject finished")
	c.lock.Unlock()
}

// GetExpiration returns when the cached object expires
func (c *Cache[T]) GetExpiration() time.Time {
	c.lock.RLock()
	defer c.lock.RUnlock()
	return c.expires
}
