package utils

import (
	"os"
	"strconv"
	"testing"
)

type testConfigJson struct {
	Value int    `json:"value" default:"3" env:"SHLVL"`
	Text  string `json:"text" default:"none" env:"HOME"`
}

func TestConfigParser1(t *testing.T) {
	c, errs := ConfigParser[testConfigJson](ParseConfigJsonFile[testConfigJson]("configv2_1.json"))
	if len(errs) != 0 {
		t.Error(errs)
		return
	}
	if c.Value != 1 {
		t.Error("expected 1, got", c.Value)
	}
	if len(c.Text) == 0 {
		t.Error("did not parse text")
	}
}

func TestConfigParser2(t *testing.T) {
	c, errs := ConfigParser[testConfigJson](
		ParseConfigJsonFile[testConfigJson]("configv2_1.json"),
		ParseConfigJsonFile[testConfigJson]("configv2_2.json"),
	)
	if len(errs) != 0 {
		t.Error(errs)
		return
	}
	if c.Value != 2 {
		t.Error("expected 2, got", c.Value)
	}
	if len(c.Text) == 0 {
		t.Error("did not parse text")
	}
}

func TestParseConfigDefaults(t *testing.T) {
	config, err := ParseConfigDefaults[testConfigJson]()(testConfigJson{})
	if err != nil {
		t.Error(err)
		return
	}
	if config.Value != 3 {
		t.Error("value should be 3")
	}
}

func TestParseConfigEnvironment(t *testing.T) {
	config, err := ParseConfigEnvironment[testConfigJson]()(testConfigJson{})
	if err != nil {
		t.Error(err)
		return
	}
	shlvl := os.Getenv("SHLVL")
	num, err := strconv.Atoi(shlvl)
	if err == nil {
		if config.Value != num {
			t.Error("value was not correct, expected", num, "got", config.Value)
		}
	} else {
		t.Log("failed to read SHLVL", err)
	}
	home := os.Getenv("HOME")
	if config.Text != home {
		t.Error("text had wrong home")
	}
}
