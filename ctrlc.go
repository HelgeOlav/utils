package utils

import (
	"os"
	"os/signal"
	"sync"
	"syscall"
)

var ctrlConce sync.Once
var ctrlCdoneCh chan struct{}

// CtrlCchannel returns a channel that is closed when pressing CTRL+C
func CtrlCchannel() chan struct{} {
	ctrlConce.Do(func() {
		ctrlCdoneCh = make(chan struct{}, 1)
		sigs := make(chan os.Signal, 1)
		signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
		go func() {
			<-sigs
			close(ctrlCdoneCh)
		}()
	})
	return ctrlCdoneCh
}
