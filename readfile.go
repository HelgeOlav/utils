package utils

import (
	"bufio"
	"os"
)

// readLines reads a whole file into memory
// and returns a slice of its lines.
// source: https://stackoverflow.com/questions/5884154/read-text-file-into-string-array-and-write
func ReadLines(path string, skipBlank bool) ([]string, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var lines []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if !(skipBlank && len(line) == 0) {
			lines = append(lines, line)
		}
	}
	return lines, scanner.Err()
}
