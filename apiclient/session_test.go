package apiclient

import (
	"bitbucket.org/HelgeOlav/utils"
	"context"
	"errors"
	"log"
	"net/http"
	"testing"
)

func logARequest(r *http.Request) error {
	log.Println(r.RequestURI)
	return nil
}

func TestSession_NewRequestSimple(t *testing.T) {
	session := NewSession("https://v2.jokeapi.dev/joke")
	resp, err := session.
		NewRequest(http.MethodGet, "Any?type=single").
		WithContext(context.TODO()).
		Do()
	if err != nil {
		t.Error(err)
		return
	}
	if resp.StatusCode != http.StatusOK {
		t.Errorf("got %d, want %d", resp.StatusCode, http.StatusOK)
	}
	// test 2
	req := session.NewRequest(http.MethodGet, "Any?type=single")
	joke, _, err := RequestWithResponse[utils.Sv443Joke](req)
	if err != nil {
		t.Error(err)
	}
	log.Println(joke.Joke)
	// test with a context that is cancelled
	myCtx, cancel := context.WithCancel(context.Background())
	cancel()
	resp, err = session.
		NewRequest(http.MethodGet, "Any?type=single").
		WithContext(myCtx).
		WithPreHook(logARequest).
		Do()
	if err != nil && !errors.Is(err, context.Canceled) {
		t.Error(err)
	} else if err == nil {
		t.Error("context was cancelled")
	}
}
