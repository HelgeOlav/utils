package apiclient

import (
	"net/http"
)

const (
	AuthorizationHeader = "Authorization"
	AuthToken           = "Token"
	AuthBearer          = "Bearer"
)

type authorizationAuth struct {
	kind  string // the kind of authorization
	value string // the value
}

func (a *authorizationAuth) SetOption(session *Session) {
	session.auth = a
}

func (a *authorizationAuth) AddAuth(r *http.Request) error {
	r.Header.Set(AuthorizationHeader, a.kind+" "+a.value)
	return nil
}

// WithAuthorization adds "Authorization: kind value" to the request
func WithAuthorization(kind, value string) Option {
	return &authorizationAuth{
		kind:  kind,
		value: value,
	}
}
