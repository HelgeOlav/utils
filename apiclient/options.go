package apiclient

import "net/http"

// Option defines an Option to the session object
type Option interface {
	SetOption(session *Session) // sets an option
}

type sessionHeaders struct {
	headers map[string]string
}

func (s *sessionHeaders) SetOption(session *Session) {
	if session.headers == nil {
		session.headers = s.headers
	} else {
		for k, v := range s.headers {
			session.headers[k] = v
		}
	}
}

func WithHeaders(headers map[string]string) Option {
	return &sessionHeaders{headers: headers}
}

type httpClient struct {
	client *http.Client
}

func (c *httpClient) SetOption(session *Session) {
	session.client = c.client
}

func WithClient(client *http.Client) Option {
	return &httpClient{client: client}
}

type hook struct {
	preHook  func(*http.Request) error
	postHook func(*http.Response) error
}

func (h *hook) SetOption(session *Session) {
	if h.preHook != nil {
		if session.preHook != nil {
			session.preHook = append(session.preHook, h.preHook)
		} else {
			session.preHook = []func(*http.Request) error{h.preHook}
		}
	}
	if h.postHook != nil {
		if session.postHook != nil {
			session.postHook = append(session.postHook, h.postHook)
		} else {
			session.postHook = []func(*http.Response) error{h.postHook}
		}
	}
}

// WithPreHook adds a hook that is called just before the request
func WithPreHook(preHook func(*http.Request) error) Option {
	return &hook{preHook: preHook}
}

// WithPostHook adds a hook that is called just after we got the response
func WithPostHook(postHook func(*http.Response) error) Option {
	return &hook{postHook: postHook}
}
