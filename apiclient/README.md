# API client

API client is a helper for working with REST APIs that you consume. It provides means for authentication, session management
and requests/parsing of payload.

## Authentication

To implement authentication, just add the appropriate authentication type when you create the session object.
If the existing authentication methods does not suit your need, you can either extend with your own type, or just
add the headers that are needed as part of the request.

These authentication schemes are supported out of the box:

* basic auth
* authorization

Its extensible model allows for adaption to almost any kind of authentication.

## Example

This example sets the base URL, with no extra options. In the request we add some parameters, resulting in
https://v2.jokeapi.dev/joke/Any?type=single being called.

```
session := NewSession("https://v2.jokeapi.dev/joke")
resp, err := session.
	NewRequest(http.MethodGet, "Any?type=single").
	WithContext(context.TODO()).
	Do()
```

In this example we get the response parsed before it is returned. This is nice if the response struct is known in advance.

```
session := NewSession("https://v2.jokeapi.dev/joke")
req := session.NewRequest(http.MethodGet, "Any?type=single")
joke, resp, err := RequestWithResponse[utils.Sv443Joke](req)
```