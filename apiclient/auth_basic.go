package apiclient

import "net/http"

// basicAuth adds basic auth to a request
type basicAuth struct {
	username, password string
}

// AddAuth adds basic auth to the request
func (b *basicAuth) AddAuth(r *http.Request) error {
	if r != nil {
		r.SetBasicAuth(b.username, b.password)
	}
	return nil
}

// SetOption set the option on the session
func (b *basicAuth) SetOption(s *Session) {
	s.auth = b
}

// WithBasicAuth returns an option that uses strict username and password
func WithBasicAuth(username, password string) Option {
	return &basicAuth{
		username: username,
		password: password,
	}
}
