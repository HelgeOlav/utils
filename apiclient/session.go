package apiclient

import (
	"net/http"
	"net/url"
)

// This file defines the session structure

type Session struct {
	baseUrl  string                           // this is the base part of the url
	headers  map[string]string                // these headers are added to all requests
	auth     Auth                             // our method to handle authentication
	client   *http.Client                     // the client we will use
	preHook  []func(req *http.Request) error  // these hooks are called just before performing the request, error aborts the request
	postHook []func(req *http.Response) error // these hooks are called after the request succeeded
}

// NewSession creates a new session object
func NewSession(baseUrl string, opts ...Option) *Session {
	s := &Session{
		baseUrl: baseUrl,
	}
	for _, opt := range opts {
		opt.SetOption(s)
	}
	return s
}

// NewRequest creates a new request object to perform a request on
func (s *Session) NewRequest(method, urlStr string) *Request {
	outUrl, _ := url.JoinPath(s.baseUrl, urlStr)
	return &Request{
		method:  method,
		session: s,
		url:     outUrl,
	}
}
