package apiclient

import (
	"bytes"
	"context"
	"encoding/json"
	"io"
	"log"
	"net/http"
)

// Request represents a request against the server
type Request struct {
	session      *Session                         // a reference to our Session
	ctx          context.Context                  // the context we will use
	method       string                           // http method
	url          string                           // computed url
	extraHeaders map[string]string                // extra headers to add to the request
	body         io.Reader                        // a body
	preHook      []func(req *http.Request) error  // these hooks are called just before performing the request, error aborts the request
	postHook     []func(req *http.Response) error // these hooks are called after the request succeeded
}

// prepareRequest is an internal helper that modifies the request object before use.
// skipAuth is used to handle authentication requests that should be without authentication.
func (r *Request) prepareRequest(req *http.Request, skipAuth bool) error {
	// add headers and auth
	for k, v := range r.session.headers {
		req.Header.Set(k, v)
	}
	for k, v := range r.extraHeaders {
		req.Header.Set(k, v)
	}
	if !skipAuth {
		if r.session.auth != nil {
			err := r.session.auth.AddAuth(req)
			if err != nil {
				return err
			}
		}
	}
	return nil
}

// Do perform the request and returns the response
func (r *Request) Do() (*http.Response, error) {
	var client *http.Client
	// get a client object
	if r.session.client == nil {
		client = http.DefaultClient
	} else {
		client = r.session.client
	}
	// make the request object
	var req *http.Request
	var err error
	if r.ctx == nil {
		req, err = http.NewRequest(r.method, r.url, r.body)
	} else {
		req, err = http.NewRequestWithContext(r.ctx, r.method, r.url, r.body)
	}
	if err != nil {
		return nil, err
	}
	// add headers and execute
	err = r.prepareRequest(req, false)
	if err != nil {
		return nil, err
	}
	for _, h := range r.session.preHook {
		if err := h(req); err != nil {
			return nil, err
		}
	}
	for _, h := range r.preHook {
		if err := h(req); err != nil {
			return nil, err
		}
	}
	resp, err := client.Do(req)
	if err == nil {
		for _, h := range r.postHook {
			if err = h(resp); err != nil {
				return resp, err
			}
		}
		for _, h := range r.session.postHook {
			if err = h(resp); err != nil {
				return resp, err
			}
		}
	}
	return resp, err
}

// WithBody adds a body to the request
func (r *Request) WithBody(body io.Reader) *Request {
	r.body = body
	return r
}

// WithJsonBody tries to JSON marshal the body and add it to the request. If it fails, an error is printed to
// the console.
func (r *Request) WithJsonBody(body interface{}) *Request {
	bb, err := json.Marshal(body)
	if err != nil {
		log.Println(err)
	} else {
		r.body = bytes.NewBuffer(bb)
	}
	return r
}

// WithExtraHeaders adds extra headers to this request. They are added after the headers from the session object.
func (r *Request) WithExtraHeaders(headers map[string]string) *Request {
	r.extraHeaders = headers
	return r
}

// WithContext adds a context to the request.
func (r *Request) WithContext(ctx context.Context) *Request {
	r.ctx = ctx
	return r
}

// RequestWithResponse performs a request, parses the response body and returns it.
func RequestWithResponse[T any](r *Request) (response T, httpResponse *http.Response, err error) {
	var resp *http.Response
	resp, err = r.Do()
	if err != nil {
		return
	}
	var bb []byte
	bb, err = io.ReadAll(resp.Body)
	_ = resp.Body.Close()
	if err != nil {
		return
	}
	err = json.Unmarshal(bb, &response)
	return
}

func (r *Request) WithPreHook(hook func(req *http.Request) error) *Request {
	r.preHook = append(r.preHook, hook)
	return r
}

func (r *Request) WithPostHook(hook func(req *http.Response) error) *Request {
	r.postHook = append(r.postHook, hook)
	return r
}
