package apiclient

import (
	"net/http"
)

// Auth represent different kinds of authentication.
type Auth interface {
	AddAuth(r *http.Request) error // this adds authentication to the requested request object
}
