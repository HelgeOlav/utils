package utils

import (
	"fmt"
	"log"
	"sync"
	"sync/atomic"
	"testing"
	"time"
)

var testCacheCounter atomic.Int64

func TestNewCache(t *testing.T) {
	fmt.Println("starter")
	cache := NewCache[int](time.Millisecond*10, -1, func() (int, bool) {
		time.Sleep(time.Millisecond * 20)
		newVal := testCacheCounter.Add(1)
		return int(newVal), true
	})
	exitChan := make(chan struct{})
	go func() {
		ticker := time.NewTicker(time.Millisecond)
		for {
			select {
			case <-ticker.C:
				_ = cache.GetValue()
			case <-exitChan:
				ticker.Stop()
				return
			}
		}
	}()
	go func() {
		ticker := time.NewTicker(time.Millisecond * 7)
		for {
			select {
			case <-exitChan:
				ticker.Stop()
				return
			case <-ticker.C:
				cache.Expire()
			}
		}
	}()
	time.Sleep(time.Second)
	close(exitChan)
	if testCacheCounter.Load() == 0 {
		t.Error("no cache iterations")
	} else {
		fmt.Println("cache iterations:", testCacheCounter.Load())
	}
}

func TestCacheRefresh(t *testing.T) {
	var counter int64
	val := NewCache[int](time.Second, -1, func() (int, bool) {
		n := atomic.AddInt64(&counter, 1)
		return int(n), true
	})
	const runTime = 5
	timer := time.NewTimer(time.Second * runTime) // run for 5 seconds
	// routine to fetch updates
	go func() {
		ticker := time.NewTicker(time.Millisecond * 800)
		for {
			select {
			case <-ticker.C:
				val.GetValue()
			case <-timer.C:
				ticker.Stop()
				return
			}
		}
	}()
	<-timer.C
	if counter == 0 {
		t.Error("counter is 0")
	}
	if counter > runTime {
		t.Errorf("counter to high with %v", counter)
	}
}

func TestNewCacheWithoutInitial(t *testing.T) {
	const numRuns = 18
	cache := NewCacheWithoutInitialValue[int](time.Millisecond*10, -1, func() (int, bool) {
		time.Sleep(time.Millisecond * 20)
		newVal := testCacheCounter.Add(1)
		return int(newVal), newVal%2 == 0
	})
	for x := 1; x < numRuns; x++ {
		log.Printf("run #1 iteration %v\n", x)
		v := cache.GetValue()
		time.Sleep(time.Millisecond * 9)
		if v == 0 {
			t.Error("Got 0 value from cache")
			return
		}
		log.Printf("Got value %v from cache on iteration %v\n", v, x)
	}
	// now try concurrent runs
	var wg sync.WaitGroup
	wg.Add(3 * numRuns)
	for i := 0; i < 3*numRuns; i++ {
		go func(id int) {
			log.Printf("concurrent id %v\n", id)
			time.Sleep(time.Millisecond * 7)
			_ = cache.GetValue()
			wg.Done()
		}(i)
	}
	wg.Wait()
}
