package utils

import (
	"log/slog"
	"testing"
)

func TestDiscardSlog(t *testing.T) {
	// test: do not panic
	logger := slog.New(DiscardSlog{})
	logger.Error("an error")
	logger.Info("some info")
	logger.Warn("a warning")
}
