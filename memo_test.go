package utils

import (
	"reflect"
	"sync/atomic"
	"testing"
)

func TestMemo_Get(t *testing.T) {
	var counter atomic.Int64
	cb := func(_ int) int64 {
		return counter.Add(1)
	}
	memo := NewMemo[int64, int](cb, 1)
	type testCase struct {
		name string
		send int
		want int64
	}
	tests := []testCase{
		{"Test 1", 1, 1},
		{"test 2", 1, 1},
		{"Test 3", 2, 2},
		{"Test 4", 2, 2},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := memo.Get(tt.send); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Get() = %v, want %v", got, tt.want)
			}
		})
	}
}
