package dlock

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"testing"
)

func TestDlockServer_cleanup(t *testing.T) {
	lock := NewDLockServer()
	const resourceA = "lockA"
	lock.Lock(resourceA)
	// update epoch and check
	lock.curEpoch++
	lock.cleanup()
	if !lock.IsPresent(resourceA) {
		t.Error("lockA should not have been cleaned up")
	}
	// update epoch and check again
	lock.curEpoch++
	lock.cleanup()
	if lock.IsPresent(resourceA) {
		t.Error("lockA should have been cleaned up")
	}
}

func TestDlockServer(t *testing.T) {
	lock := NewDLockServer()
	log.Println("TestDlockServer started")
	const resourceA = "lockA"
	if lock.IsPresent(resourceA) {
		t.Error("lockA is present")
		return
	}
	if !lock.Lock(resourceA) {
		t.Error("lockA was not locked")
		return
	}
	if lock.RLock(resourceA) {
		t.Error("lockA was given r/o lock")
		return
	}
	if lock.Count() != 1 {
		t.Error("lock does not contain exactly 1 item")
	}
	// attempt to lock it again
	if lock.Lock(resourceA) {
		t.Error("lockA was locked twice")
		return
	}
	if !lock.IsPresent(resourceA) {
		t.Error("lockA is not present when it should")
		return
	}
	// unlock it
	if !lock.Unlock(resourceA) {
		t.Error("lockA was not unlocked")
		return
	}
	// do concurrent read locks
	if !lock.RLock(resourceA) {
		t.Error("lockA was not r-locked")
		return
	}
	if !lock.RLock(resourceA) {
		t.Error("lockA was not r-locked")
		return
	}
	if lock.Lock(resourceA) {
		t.Error("lockA was given rw lock when it was r-locked")
		return
	}
	if !lock.RUnlock(resourceA) {
		t.Error("RUnlock failed #1")
		return
	}
	if !lock.RUnlock(resourceA) {
		t.Error("RUnlock failed #2")
		return
	}
	if lock.RUnlock(resourceA) {
		t.Error("RUnlock succeeded #3")
		return
	}
	// continue
	if lock.Unlock(resourceA) {
		t.Error("lockA was unlocked when not existing")
		return
	}
	if lock.IsPresent(resourceA) {
		t.Error("lockA is present when it should not")
		return
	}
}

func TestLockClient(t *testing.T) {
	const resourceA = "lockA"
	serverLock := NewDLockServer(WithPrefix("/lock/"))
	// start http server
	mux := http.NewServeMux()
	mux.Handle("/lock/", serverLock)
	listenPort := "localhost:8899"
	server := http.Server{Addr: listenPort, Handler: mux}
	go server.ListenAndServe()
	// create client
	lock := NewDlockClient(fmt.Sprintf("http://%s/lock", listenPort))
	WithClientContext(context.TODO())(lock) // if you want to replace the context
	// try to lock
	if lock.IsPresent(resourceA) {
		t.Error("lockA is present")
		return
	}
	if !lock.Lock(resourceA) {
		t.Error("lockA was not locked")
		return
	}
	if lock.RLock(resourceA) {
		t.Error("lockA ro succeeded when it should have failed")
	}
	if !serverLock.IsPresent(resourceA) {
		t.Error("lpc call succeeded when it should have failed")
	}
	// attempt to lock it again
	if lock.Lock(resourceA) {
		t.Error("lockA was locked twice")
		return
	}
	if !lock.IsPresent(resourceA) {
		t.Error("lockA is not present when it should")
		return
	}
	// unlock it
	if !lock.Unlock(resourceA) {
		t.Error("lockA was not unlocked")
		return
	}
	if lock.Unlock(resourceA) {
		t.Error("lockA was unlocked twice")
		return
	}
	if lock.IsPresent(resourceA) {
		t.Error("lockA is present when it should not")
		return
	}
	// try read locks
	if !lock.RLock(resourceA) {
		t.Error("RLock failed #1")
		return
	}
	if !lock.RLock(resourceA) {
		t.Error("RLock failed #2")
		return
	}
	if lock.Lock(resourceA) {
		t.Error("Lock succeeded after RLock")
		return
	}
	if !lock.RUnlock(resourceA) {
		t.Error("RUnlock failed #1")
	}
	if !lock.RUnlock(resourceA) {
		t.Error("RUnlock failed #2")
	}
	if lock.RUnlock(resourceA) {
		t.Error("RUnlock failed #3")
	}
}
