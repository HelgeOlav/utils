package dlock

import (
	"bitbucket.org/HelgeOlav/utils"
	"log/slog"
	"net/http"
	"strings"
	"sync"
	"time"
)

/*
Distributed lock: locks and unlocks resources based on HTTP requests, is built as a minimal distributed lock for low-volume requests/locks.
Supports the following methods:
* GET - to check if a lock is in place (204 on success, 404 if it does not exist)
* PUT - create a lock, will only succeed if the lock does not exist (201 if created, 409 if already exist)
* DELETE - will delete a lock (404 if lock does not exist, 200 if deleted)

A cleanup task is run every hour (from startup), and locks are removed after two cleanups. They will stay up to three hours if you do not remove them yourself.
*/

// DLocker is an interface that can represent both the server and the client after startup
type DLocker interface {
	Lock(string) bool      // lock an object, returns true if you got the lock
	Unlock(string) bool    // unlock and object, returns true if you unlocked it
	RLock(string) bool     // lock read-only (multiple locks supported)
	RUnlock(string) bool   // unlock a read-only lock
	IsPresent(string) bool // returns true if the object is present and locked
}

// DLockServer is a distributed lock
type DLockServer struct {
	lock            sync.RWMutex          // lock concurrent read and write-access to map
	curEpoch        int                   // current time epoch for locks
	locks           map[string]dLockEntry // our list of locks
	cleanupInterval time.Duration         // how often cleanup should run, 0 = disable
	logger          *slog.Logger          // what logger we should log on
	prefix          string                // a prefix that will be removed from the lock name before it is stored. This can be used to let remote and local calls work together. It is only used in the ServerHTTP call.
	prefixLen       int                   // length of the prefix
}

// dLockEntry is the details about one lock
type dLockEntry struct {
	Rlocks int64 // number of read locks
	Wlocks int64 // number of write locks, should only be 0 or 1
	epoch  int   // what epoch this lock was made in
}

const (
	LoggerName  = "name"  // name variable for logging
	LoggerLock  = "lock"  // name for lock status
	LoggerValue = "value" // value for a number
	roQuery     = "?lock=true"
)

// ServerOption is used for passing options into DLockServer
type ServerOption func(server *DLockServer)

// CleanupInterval defines how often to clean up stale locks
func CleanupInterval(interval time.Duration) ServerOption {
	return func(d *DLockServer) {
		d.cleanupInterval = interval
	}
}

// WithPrefix specifies a prefix that is removed from all HTTP calls before stored into memory.
// When used, you can do both local and remote calls at the same time on the same object name.
func WithPrefix(prefix string) ServerOption {
	return func(d *DLockServer) {
		d.prefix = prefix
		d.prefixLen = len(prefix)
	}
}

// WithLogger specifies what logger to use
func WithLogger(logger *slog.Logger) ServerOption {
	return func(d *DLockServer) {
		d.logger = logger
	}
}

// NewDLockServer returns a new instance of a lock
func NewDLockServer(opts ...ServerOption) *DLockServer {
	defaultOps := []ServerOption{
		CleanupInterval(time.Hour),
		WithLogger(slog.New(utils.DiscardSlog{})),
	}
	opts = append(defaultOps, opts...)
	server := &DLockServer{locks: make(map[string]dLockEntry)}
	for _, o := range opts {
		o(server)
	}
	go server.backgroundtask()
	return server
}

// cleanup removes stale objects when called
func (d *DLockServer) cleanup() {
	d.lock.RLock()
	var objects []string
	for x := range d.locks {
		if d.curEpoch-d.locks[x].epoch > 1 {
			objects = append(objects, x)
		}
	}
	d.lock.RUnlock()
	// now delete
	d.lock.Lock()
	d.curEpoch++
	for x := range objects {
		delete(d.locks, objects[x])
	}
	d.lock.Unlock()
	// and log
	for x := range objects {
		d.logger.Warn("DLock", objects[x], "deleted on timeout")
	}
}

// backgroundtask cleans up stale locks that has been there for too long
func (d *DLockServer) backgroundtask() {
	if d.cleanupInterval == 0 {
		return
	}
	ticker := time.NewTicker(d.cleanupInterval)
	for {
		select {
		case <-utils.CtrlCchannel():
			ticker.Stop()
			return
		case <-ticker.C:
			d.cleanup()
		}
	}
}

// IsPresent checks if a lock is present and returns the result
func (d *DLockServer) IsPresent(name string) bool {
	d.lock.RLock()
	_, ok := d.locks[name]
	d.lock.RUnlock()
	d.logger.Debug("IsPresent", LoggerName, name, LoggerLock, ok)
	return ok
}

// Count returns the number of objects that are locked
func (d *DLockServer) Count() int {
	d.lock.RLock()
	c := len(d.locks)
	d.lock.RUnlock()
	d.logger.Info("Count", LoggerValue, c)
	return c
}

// RLock returns true if a read lock could be obtained. Multiple read locks can be given.
func (d *DLockServer) RLock(name string) (result bool) {
	d.lock.Lock()
	defer d.lock.Unlock()
	lock, ok := d.locks[name]
	if ok {
		if lock.Wlocks > 0 {
			return false
		}
		lock.Rlocks++
		d.locks[name] = lock
	} else {
		d.locks[name] = dLockEntry{epoch: d.curEpoch, Rlocks: 1}
	}
	return true
}

// RUnlock unlocks a read lock
func (d *DLockServer) RUnlock(name string) (result bool) {
	d.lock.Lock()
	defer d.lock.Unlock()
	lock, ok := d.locks[name]
	if ok {
		if lock.Wlocks == 0 && lock.Rlocks > 0 {
			lock.Rlocks--
			if lock.Rlocks > 0 {
				d.locks[name] = lock
			} else {
				delete(d.locks, name)
			}
			return true
		}
	}
	return false
}

// Lock creates a lock, returns true if lock was created. On false the lock already exist.
func (d *DLockServer) Lock(name string) (result bool) {
	d.lock.Lock()
	// see if lock is present, abort if it is
	_, ok := d.locks[name]
	if ok {
		// lock was not acquired
		result = false
	} else {
		// create new lock
		d.locks[name] = dLockEntry{epoch: d.curEpoch, Wlocks: 1}
		result = true
	}
	d.lock.Unlock()
	d.logger.Debug("Lock", LoggerName, name, LoggerLock, result)
	return
}

// Unlock unlocks the lock, returning true if the lock was unlocked, false if it does not exist.
// An Unlock will also delete any read locks.
func (d *DLockServer) Unlock(name string) (result bool) {
	d.lock.Lock()
	// see if lock is absent, abort if it is
	_, ok := d.locks[name]
	if !ok {
		result = false
	} else {
		// create new lock
		delete(d.locks, name)
		result = true
	}
	d.lock.Unlock()
	d.logger.Debug("Unlock", LoggerName, name, LoggerLock, result)
	return
}

// ServeHTTP is our HTTP handler. Any logging and authentication here should be done using a middleware.
func (d *DLockServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	objectName := r.URL.Path // the name of the object we will lock
	roLockString := r.URL.Query().Get("lock")
	roLock := roLockString == "true"
	var result bool
	if d.prefixLen > 0 && strings.HasPrefix(objectName, d.prefix) {
		objectName = objectName[d.prefixLen:]
	}
	// check method, and handle accordingly
	switch r.Method {
	case http.MethodGet:
		result := d.IsPresent(objectName)
		if result {
			w.WriteHeader(http.StatusNoContent)
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	case http.MethodPut:
		if roLock {
			result = d.RLock(objectName)
		} else {
			result = d.Lock(objectName)
		}
		if result {
			w.WriteHeader(http.StatusCreated)
		} else {
			w.WriteHeader(http.StatusConflict)
		}
	case http.MethodDelete:
		if roLock {
			result = d.RUnlock(objectName)
		} else {
			result = d.Unlock(objectName)
		}
		if result {
			w.WriteHeader(http.StatusOK)
		} else {
			w.WriteHeader(http.StatusNotFound)
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
	}
}
