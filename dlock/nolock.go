package dlock

// DNoLocker is a locker that never locks and always succeeds.
// This is meant for scenarios where you don't need a real lock, and just need a placeholder.
type DNoLocker struct {
}

func (d *DNoLocker) Lock(_ string) bool {
	return true
}

func (d *DNoLocker) Unlock(_ string) bool {
	return true
}

func (d *DNoLocker) RLock(_ string) bool {
	return true
}

func (d *DNoLocker) RUnlock(_ string) bool {
	return true
}

func (d *DNoLocker) IsPresent(_ string) bool {
	return false
}
