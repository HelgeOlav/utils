package dlock

import (
	"bitbucket.org/HelgeOlav/utils"
	"context"
	"log"
	"log/slog"
	"net/http"
	"net/url"
)

/*
DLockClient is a REST based client that talks to an HTTP server to do locking/unlocking of objects.
*/

// DLockClient represents a client that connects to a server
type DLockClient struct {
	basePath string // base path for locking/unlocking
	logger   *slog.Logger
	client   *http.Client
	headers  map[string]string
	ctx      context.Context
}

// ClientOption is used for passing options into DLockServer
type ClientOption func(server *DLockClient)

// WithClientContext defines a context to be used for all requests.
// As an alternative you can define your own http.Client and configure timeout there.
func WithClientContext(ctx context.Context) ClientOption {
	return func(d *DLockClient) {
		d.ctx = ctx
	}
}

// WithClientHeaders configures a map of headers to add to all requests
func WithClientHeaders(h map[string]string) ClientOption {
	return func(d *DLockClient) {
		d.headers = h
	}
}

// WithClientLogger configures a logger
func WithClientLogger(logger *slog.Logger) ClientOption {
	return func(d *DLockClient) {
		d.logger = logger
	}
}

// WithClient configures the HTTP Client to use
func WithClient(c *http.Client) ClientOption {
	return func(d *DLockClient) {
		d.client = c
	}
}

// addHeaders adds all headers to a request object
func (d *DLockClient) addHeaders(r *http.Request) {
	if d.headers != nil {
		for k, v := range d.headers {
			r.Header.Add(k, v)
		}
	}
}

// NewDlockClient returns a new client
func NewDlockClient(base string, opts ...ClientOption) *DLockClient {
	c := &DLockClient{basePath: base}
	defOpts := []ClientOption{
		WithClientLogger(slog.New(utils.DiscardSlog{})),
		WithClient(http.DefaultClient),
		WithClientContext(context.Background()),
	}
	opts = append(defOpts, opts...)
	for _, o := range opts {
		o(c)
	}
	return c
}

// Lock tries to acquire a read/write lock
func (d *DLockClient) Lock(name string) bool {
	return d.lock(name, false)
}

// RLock tries to acquire a read-only lock
func (d *DLockClient) RLock(name string) bool {
	return d.lock(name, true)
}

// lock returns true if an object was locked, false otherwise including when server is not available
func (d *DLockClient) lock(name string, ro bool) bool {
	d.logger.Debug("lock", LoggerName, name)
	u, err := url.JoinPath(d.basePath, name)
	if err != nil {
		log.Println(err)
		return false
	}
	if ro {
		u = u + roQuery
	}
	req, err := http.NewRequestWithContext(d.ctx, http.MethodPut, u, nil)
	if err != nil {
		log.Println(err)
		return false
	}
	d.addHeaders(req)
	res, err := d.client.Do(req)
	if err != nil {
		log.Println(err)
		return false
	}
	if res.StatusCode == http.StatusCreated {
		return true
	}
	return false
}

// Unlock releases a r/w lock
func (d *DLockClient) Unlock(name string) bool {
	return d.unlock(name, false)
}

// RUnlock releases a r/o lock
func (d *DLockClient) RUnlock(name string) bool {
	return d.unlock(name, true)
}

// unlock unlocks a lock
func (d *DLockClient) unlock(name string, ro bool) bool {
	d.logger.Debug("Unlock", LoggerName, name)
	u, err := url.JoinPath(d.basePath, name)
	if err != nil {
		log.Println(err)
		return false
	}
	if ro {
		u = u + roQuery
	}
	req, err := http.NewRequestWithContext(d.ctx, http.MethodDelete, u, nil)
	if err != nil {
		log.Println(err)
		return false
	}
	d.addHeaders(req)
	res, err := d.client.Do(req)
	if err != nil {
		log.Println(err)
		return false
	}
	if res.StatusCode == http.StatusOK {
		return true
	}
	return false
}

// IsPresent checks if the lock is present
func (d *DLockClient) IsPresent(name string) bool {
	d.logger.Debug("IsPresent", LoggerName, name)
	u, err := url.JoinPath(d.basePath, name)
	if err != nil {
		log.Println(err)
		return false
	}
	req, err := http.NewRequestWithContext(d.ctx, http.MethodGet, u, nil)
	if err != nil {
		log.Println(err)
		return false
	}
	d.addHeaders(req)
	res, err := d.client.Do(req)
	if err != nil {
		log.Println(err)
		return false
	}
	if res.StatusCode == http.StatusNoContent {
		return true
	}
	return false
}
