# Distributed lock

The idea about this package is to provide some means to lock an object. The object is your arbitrary chosen name.
This implementation is inspired by [redislock](https://github.com/bsm/redislock), but aims to be simpler by not requiring redis and is written completely in Golang. 

The name implies that this is a distributed lock, and that is handles by providing a REST API (and the golang client, so you can use it in a distributed manner).
There is also a [gRPC](https://bitbucket.org/HelgeOlav/dlock_grpc) implementation if you prefer to use that instead.

Three methods are exposed:
* Lock - returns true if the lock was acquired
* Unlock - returns true if the lock was released
* IsPresent - returns true if the lock exists

They all take one argument - the name of the lock and returns true if successful/present.

## To run only locally

```go
lock := dlock.NewDLockServer()
if !lock.Lock("myObject") {
	panic("lock not obtained")
}
```

## To run as a server

```go
lock := dlock.NewDLockServer(dlock.WithPrefix("/lock/"))
http.Handle("/lock/", lock)
http.ListenAndServe(":8080", nil)
```

Now you can connect your client.
When using WithPrefix as shown above you can use the lock both locally and remote at the same time.
(By default the lock that is acquired is the full request URL.)

## To run as a client

```go
lock := dlock.DLockClient{basePath: "http://localhost:8080/lock"}
if !lock.Lock("myObject") {
    panic("lock not obtained")
}

If the server is not available, then all calls will return false.
```

There are options you can add to include extra headers, use your own context etc.

## Authentication

If you want to add authentication you will need to add this as headers on the client.
On the server that is handled using some kind of middleware.