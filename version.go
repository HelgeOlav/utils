package utils

import "bitbucket.org/HelgeOlav/utils/version"

const myVersion = "0.2.15"

func init() {
	v := version.ModuleVersion{Name: "bitbucket.org/HelgeOlav/utils", Version: myVersion + version.DEVSUFFIX}
	version.AddModule(v)
}
