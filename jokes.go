package utils

import (
	"context"
	"fmt"
	"math/rand"
	"strconv"
)

// Joke is a joke as returned from the API https://api.chucknorris.io/
type Joke struct {
	Icon           string `json:"icon_url"`
	ID             string `json:"id"`
	URL            string `json:"url"`
	Value          string `json:"value"`
	SourceProvider int    `json:"sourceProvider"` // internal value, indicating what provider it os from
}

const (
	JokeProviderChuckNorris = 0
	JokeProviderJokeAPI     = 1
	JokeProviderLocal       = 2 // should be last element
)

// Sv443Joke is the JSON from https://sv443.net/jokeapi/v2/
type Sv443Joke struct {
	Error          bool     `json:"error"`                    // should be false
	InternalError  bool     `json:"internalError,omitempty"`  // when Error==true
	Code           int      `json:"code,omitempty"`           // when Error==true
	Message        string   `json:"message,omitempty"`        // when Error==true
	CausedBy       []string `json:"causedBy,omitempty"`       // when Error==true
	AdditionalInfo string   `json:"additionalInfo,omitempty"` // when Error==true
	Category       string   `json:"category"`
	Type           string   `json:"type"`               // [ single, twopart ]
	Setup          string   `json:"setup,omitempty"`    // when Type == twopart
	Delivery       string   `json:"delivery,omitempty"` // when Type == twopart
	Joke           string   `json:"joke"`               // when Type == single
	Flags          struct {
		SafeForWork bool `json:"nsfw"`
		Religious   bool `json:"religious"`
		Political   bool `json:"political"`
		Racist      bool `json:"racist"`
		Sexist      bool `json:"sexist"`
		Explicit    bool `json:"explicit"`
	} `json:"flags"`
	ID   int    `json:"id"`
	Safe bool   `json:"safe"`
	Lang string `json:"lang"`
}

func (s Sv443Joke) ToJoke() Joke {
	j := Joke{SourceProvider: JokeProviderJokeAPI}
	if s.Error == false {
		j.ID = strconv.Itoa(s.ID)
		if s.Type == "single" {
			j.Value = s.Joke
		} else {
			j.Value = s.Setup + "\n" + s.Delivery
		}
		j.URL = fmt.Sprintf("https://v2.jokeapi.dev/joke/Any?idRange=%v", s.ID)
	}
	return j
}

// GetSv443Joke returns a random joke from SV443
func GetSv443Joke(ctx context.Context) (Sv443Joke, error) {
	return DownloadUrlWithContext[Sv443Joke](ctx, "https://v2.jokeapi.dev/joke/Any?type=single", nil)
}

// GetChuckNorrisJoke returns a new joke or an error on error
func GetChuckNorrisJoke() (joke Joke, err error) {
	return DownloadUrlWithContext[Joke](context.Background(), "https://api.chucknorris.io/jokes/random", nil)
}

// GetRandomJoke returns a random joke from any provider
func GetRandomJoke() (j Joke) {
	provider := rand.Intn(JokeProviderLocal) // returns a number in the range 0-2 (three in total)
	var err error
	switch provider {
	case JokeProviderChuckNorris:
		j, err = GetChuckNorrisJoke()
	case JokeProviderJokeAPI:
		var jj Sv443Joke
		jj, err = GetSv443Joke(context.Background())
		j = jj.ToJoke()
	}
	if err != nil || len(j.Value) == 0 {
		j = GetLocalJoke() // fallback to a local joke
	}
	return
}

// GetLocalJoke returns a joke from the local store
func GetLocalJoke() Joke {
	return Joke{SourceProvider: JokeProviderLocal, Value: randomJokes[rand.Int31n(int32(len(randomJokes)))]}
}

// randomJokes is a list of random jokes to print out
var randomJokes = []string{
	"Adam & Eve were the first ones to ignore the Apple terms and conditions.",
	"Will glass coffins be a success? Remains to be seen.",
	"The man who invented knock-knock jokes should get a no bell prize.",
	"I went to buy some camo pants but couldn’t find any.",
	"When life gives you melons, you might be dyslexic.",
	"I spent a lot of time, money, and effort childproofing my house… But the kids still get in.",
}

// JokeCallback is intended to be used as a callback on Cache, and returns a new joke.
func JokeCallback() (Joke, bool) {
	j := GetRandomJoke()
	return j, len(j.Value) > 0
}
