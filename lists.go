package utils

import (
	"encoding/json"
)

// IsStringInStrings checks if value is in the list and returns true if so.
//
// Deprecated: use IsValueIn instead
func IsStringInStrings(value string, list *[]string) bool {
	if list != nil {
		for x := 0; x < len(*list); x++ {
			if (*list)[x] == value {
				return true
			}
		}
	}
	return false
}

// IsIntInInts checks if value is in the list and returns true if so.
//
// Deprecated: use IsValueIn instead
func IsIntInInts(value int, list *[]int) bool {
	if list != nil {
		for x := 0; x < len(*list); x++ {
			if (*list)[x] == value {
				return true
			}
		}
	}
	return false
}

// IsValueIn is a generic call to check if the value T is in an array of that type.
func IsValueIn[T comparable](value T, list *[]T) bool {
	if list != nil {
		for x := 0; x < len(*list); x++ {
			if (*list)[x] == value {
				return true
			}
		}
	}
	return false
}

// ParseJsonFromSlice parses JSON passed as a byte slice
func ParseJsonFromSlice[T any](data []byte) (result T, err error) {
	err = json.Unmarshal(data, &result)
	return
}

// Difference compares two slices and returns a list of all items that are not in both lists
func Difference[T comparable](slice1 []T, slice2 []T) []T {
	var diff []T

	// Loop two times, first to find slice1 strings not in slice2,
	// second loop to find slice2 strings not in slice1
	for i := 0; i < 2; i++ {
		for _, s1 := range slice1 {
			found := false
			for _, s2 := range slice2 {
				if s1 == s2 {
					found = true
					break
				}
			}
			// String not found. We add it to return slice
			if !found {
				diff = append(diff, s1)
			}
		}
		// Swap the slices, only if it was the first loop
		if i == 0 {
			slice1, slice2 = slice2, slice1
		}
	}
	return diff
}

// OnlyInFirstList returns T where T is only in slice1
func OnlyInFirstList[T comparable](slice1 []T, slice2 []T) []T {
	var diff []T

	for _, s1 := range slice1 {
		// check if s1 is in slice2
		found := false
		for _, x := range slice2 {
			if s1 == x {
				found = true
				break
			}
		}
		if !found {
			diff = append(diff, s1)
		}
	}
	return diff
}

// RemoveDuplicate removes duplicates from a slice.
func RemoveDuplicate[T comparable](tSlice []T) []T {
	allKeys := make(map[T]struct{})
	list := []T{}
	for _, item := range tSlice {
		if _, value := allKeys[item]; !value {
			allKeys[item] = struct{}{}
			list = append(list, item)
		}
	}
	return list
}

// Ordered is a types that their values can be compared with < and >
type Ordered interface {
	int | int8 | int16 | int32 | int64 | float32 | float64 | uint | uint8 | uint16 | uint32 | uint64
}

// HighestValue returns the highest value in the list
func HighestValue[T Ordered](input []T) (result T) {
	for x := range input {
		if input[x] > result {
			result = input[x]
		}
	}
	return
}

// LowestValue returns the lowest value in the list
func LowestValue[T Ordered](input []T) (result T) {
	if len(input) > 0 {
		result = input[0]
	}
	for x := range input {
		if input[x] < result {
			result = input[x]
		}
	}
	return
}
