package utils

import (
	"fmt"
	"testing"
)

func TestReadJsonFile(t *testing.T) {
	type myData struct {
		Status  bool
		Message string
		Number  int
	}
	d := myData{}
	err := ReadJsonFile("config_test.json", &d)
	if err != nil {
		t.Error(err.Error())
		return
	}
	if d.Number != 4 {
		t.Error("Number is not 4, got", d.Number)
	}
	if d.Status == false {
		t.Error("Status is false, expected true")
	}
	if d.Message != "ok" {
		t.Error("Message was nok ok, got", d.Message)
	}
}

func TestLoadConfigDefaults(t *testing.T) {
	type TestData struct {
		Integer int     `default:"100" json:"integer"`
		Text    string  `default:"hello"`
		Float   float64 `default:"3.14"`
		Boolean bool    `default:"true"`
		secret  string  `default:"a field that can cause a panic"`
	}
	var data TestData
	err := LoadConfigDefaults(&data, TagDefault)
	if err != nil {
		t.Error(err)
	}
	if data.Integer != 100 {
		t.Error("parse int failed")
	}
	if data.Text != "hello" {
		t.Error("parse string failed")
	}
	if fmt.Sprintf("%.2f", data.Float) != "3.14" {
		t.Error("parse float failed, got", data.Float)
	}
	if !data.Boolean {
		t.Error("parse bool failed")
	}
	if len(data.secret) != 0 {
		t.Error("private var should be blank")
	}
}
