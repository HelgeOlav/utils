package utils

import (
	"context"
	"net/http"
	"testing"
)

func TestHttpRequest(t *testing.T) {
	// check link that exist, but does not parse
	_, _, err := HttpRequest[Joke](context.Background(), nil, http.MethodGet, "http://www.helge.net", nil)
	if err == nil {
		t.Error("should have error")
	}
	// check link that does not exist (404)
	_, _, err = HttpRequest[Joke](context.Background(), nil, http.MethodGet, "https://www.helge.net/doesnotexist", nil)
	if err == nil {
		t.Error("should have error")
	}
	// check link that is valid and can be parsed
	_, _, err = HttpRequest[Joke](context.Background(), nil, http.MethodGet, "https://api.chucknorris.io/jokes/random", nil)
	if err != nil {
		t.Error("should not have error")
	}
}

func TestDownloadUrlWithContext(t *testing.T) {
	type testCase[T any] struct {
		name    string
		link    string
		wantErr bool
	}
	tests := []testCase[Joke]{
		{name: "Link that exist but without JSON", link: "https://www.helge.net", wantErr: true},
		{name: "Link that does not exist", link: "https://www.helge.net/doesnotexist", wantErr: true},
		{name: "Invalid link", link: "http://www.helge.local/doesnotexist", wantErr: true},
		{name: "Valid link", link: "https://api.chucknorris.io/jokes/random", wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := DownloadUrlWithContext[Joke](context.Background(), tt.link, nil)
			if (err != nil) != tt.wantErr {
				t.Errorf("DownloadUrlWithContext() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
