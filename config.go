package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"reflect"
	"strconv"
)

// Various tools to read config files

// ReadJsonFile reads the content of a JSON file and parses it into data. Unless you have an error data is populated with data from the file specified.
// Deprecated: use DownloadLocalFile instead
func ReadJsonFile(fn string, data interface{}) error {
	// read config file
	bytes, err := os.ReadFile(fn)
	if err == nil {
		// parse file
		err = json.Unmarshal(bytes, data)
	}
	return err
}

// ReadConfig tries to load configuration from disk.
// Lookup is done from first to last file specified and then from the environment variable. It returns true if config is loaded.
// Any error is hidden from this call.
// Default values are also loaded.
func ReadConfig(fn []string, env string, data interface{}) bool {
	_ = LoadConfigDefaults(data, TagDefault)
	// get file from environment
	if len(env) > 0 {
		envFn := os.Getenv(env)
		if len(envFn) > 0 {
			fn = append(fn, envFn)
		}
	}
	// try to read config
	for _, v := range fn {
		err := ReadJsonFile(v, data)
		if err == nil {
			return true
		}
	}
	return false
}

const (
	TagDefault = "default" // default tag name when parsing tags in struct
	EnvDefault = "env"     // default tag for environment variables
)

// LoadConfigDefaults puts defaults values into a struct from tags. Bool, int and floats are supported,
// loading is aborted on first error
func LoadConfigDefaults(data interface{}, tag string) error {
	// validate input
	if data == nil {
		return errors.New("nil input")
	}
	if len(tag) == 0 {
		return errors.New("no tag")
	}
	v := reflect.ValueOf(data)
	if v.Kind() != reflect.Ptr {
		return errors.New("not a pointer")
	}
	v = v.Elem() // dereference pointer
	if v.Kind() != reflect.Struct {
		return errors.New("not struct")
	}
	if !v.CanSet() {
		return errors.New("values can not be set")
	}
	// loop through types
	t := v.Type()
	for i := 0; i < t.NumField(); i++ {
		sf := t.Field(i)
		value := sf.Tag.Get(tag)
		if sf.IsExported() && len(value) > 0 {
			field := v.FieldByName(sf.Name)
			if field.IsValid() && field.CanSet() {
				switch field.Kind() {
				case reflect.String:
					field.SetString(value)
				case reflect.Bool:
					val, err := strconv.ParseBool(value)
					if err != nil {
						return err
					}
					field.SetBool(val)
				case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
					val, err := strconv.ParseInt(value, 10, 32)
					if err != nil {
						return err
					}
					field.SetInt(val)
				case reflect.Uint, reflect.Uint32, reflect.Uint16, reflect.Uint64, reflect.Uint8:
					val, err := strconv.ParseUint(value, 10, 32)
					if err != nil {
						return err
					}
					field.SetUint(val)
				case reflect.Float64, reflect.Float32:
					val, err := strconv.ParseFloat(value, 32)
					if err != nil {
						return err
					}
					field.SetFloat(val)
				default:
					return fmt.Errorf("kind %s not supported", field.Kind().String())
				}
			} else {
				return fmt.Errorf("invalid field %s", sf.Name)
			}
		}
	}
	return nil
}

// loadEnvDefaults puts defaults values into a struct from the environment variable defined.
func loadEnvDefaults(data interface{}, env string) error {
	// validate input
	if data == nil {
		return errors.New("nil input")
	}
	if len(env) == 0 {
		return errors.New("no tag")
	}
	v := reflect.ValueOf(data)
	if v.Kind() != reflect.Ptr {
		return errors.New("not a pointer")
	}
	v = v.Elem() // dereference pointer
	if v.Kind() != reflect.Struct {
		return errors.New("not struct")
	}
	if !v.CanSet() {
		return errors.New("values can not be set")
	}
	// loop through types
	t := v.Type()
	for i := 0; i < t.NumField(); i++ {
		sf := t.Field(i)
		value := sf.Tag.Get(env)
		if len(value) > 0 {
			value = os.Getenv(value)
		}
		if sf.IsExported() && len(value) > 0 {
			field := v.FieldByName(sf.Name)
			if field.IsValid() && field.CanSet() {
				switch field.Kind() {
				case reflect.String:
					field.SetString(value)
				case reflect.Bool:
					val, err := strconv.ParseBool(value)
					if err != nil {
						return err
					}
					field.SetBool(val)
				case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
					val, err := strconv.ParseInt(value, 10, 32)
					if err != nil {
						return err
					}
					field.SetInt(val)
				case reflect.Uint, reflect.Uint32, reflect.Uint16, reflect.Uint64, reflect.Uint8:
					val, err := strconv.ParseUint(value, 10, 32)
					if err != nil {
						return err
					}
					field.SetUint(val)
				case reflect.Float64, reflect.Float32:
					val, err := strconv.ParseFloat(value, 32)
					if err != nil {
						return err
					}
					field.SetFloat(val)
				default:
					return fmt.Errorf("kind %s not supported", field.Kind().String())
				}
			} else {
				return fmt.Errorf("invalid field %s", sf.Name)
			}
		}
	}
	return nil
}
