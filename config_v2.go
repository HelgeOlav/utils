package utils

import (
	"encoding/json"
	"io"
	"net/http"
	"os"
)

/*
	Config v2 is a new and improved configuration handler (compared to the original version) with benefits like:
	- Getting default values from the struct
	- Reading configuration from JSON file(s) and environment variables
	- Flexible configuration order
*/

// ConfigParserOption is a method that can be called that tries to get some values into T in some way.
type ConfigParserOption[T any] func(T) (T, error)

// ConfigParser parses T and returns a new configuration object based on the various options passed in.
func ConfigParser[T any](parsers ...ConfigParserOption[T]) (T, []error) {
	var result T
	var errors []error
	for x := range parsers {
		var err error
		result, err = parsers[x](result)
		if err != nil {
			errors = append(errors, err)
		}
	}
	return result, errors
}

// ConfigParserWithDefaults allows you to specify one or more files that are read in order, after defaults and environment.
// If no file is specified then "config.json" is used as the config file.
func ConfigParserWithDefaults[T any](file ...string) (T, []error) {
	parsers := []ConfigParserOption[T]{
		ParseConfigDefaults[T](),
		ParseConfigEnvironment[T](),
	}
	if len(file) == 0 {
		file = []string{"config.json"}
	}
	for x := range file {
		parsers = append(parsers, ParseConfigJsonFile[T](file[x]))
	}
	return ConfigParser[T](parsers...)
}

// ParseConfigJsonFile reads a configuration file from disk, and merges it into the current configuration.
func ParseConfigJsonFile[T any](fn string) ConfigParserOption[T] {
	return func(t T) (T, error) {
		data, err := os.ReadFile(fn)
		if err == nil {
			err = json.Unmarshal(data, &t)
		}
		return t, err
	}
}

// ParseConfigDefaults set default values defined in a struct, should be first config option passed.
func ParseConfigDefaults[T any]() ConfigParserOption[T] {
	return func(t T) (T, error) {
		err := LoadConfigDefaults(&t, TagDefault)
		return t, err
	}
}

// ParseConfigEnvironment get values from the environment variables and put into T.
func ParseConfigEnvironment[T any]() ConfigParserOption[T] {
	return func(t T) (T, error) {
		err := loadEnvDefaults(&t, EnvDefault)
		return t, err
	}
}

// ParseConfigJsonHttp get values from HTTP
func ParseConfigJsonHttp[T any](url string, headers map[string]string) ConfigParserOption[T] {
	return func(t T) (T, error) {
		req, _ := http.NewRequest(http.MethodGet, url, nil)
		for x := range headers {
			req.Header.Set(x, headers[x])
		}
		resp, err := http.DefaultClient.Do(req)
		var body []byte
		if err == nil && resp.ContentLength > 0 && resp.StatusCode == http.StatusOK {
			body, err = io.ReadAll(resp.Body)
			_ = resp.Body.Close()
			err = json.Unmarshal(body, &t)
		}
		return t, err
	}
}
