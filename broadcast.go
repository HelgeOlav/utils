package utils

import (
	"sync"
)

// Broadcast is a tool to let you notify many receivers on a change of a value using signals.
// This is implemented using generics, so you can pass your own type in a type-safe manner.
// Note that if you have many / rapid updates to the value, the readers can get an outdated value or miss an update.
type Broadcast[T any] struct {
	value         T             // the value that we hold
	valMtx        sync.RWMutex  // our mutex for value of T
	notifyChannel chan struct{} // the channel we will close when a change occurs
	notifyMtx     sync.RWMutex  // our mutex protecting notifyChannel
	epoch         uint          // increases by each SetValue
}

// GetValue returns the current value
func (b *Broadcast[T]) GetValue() T {
	b.valMtx.RLock()
	defer b.valMtx.RUnlock()
	return b.value
}

// GetValueWithEpoch returns the current value
func (b *Broadcast[T]) GetValueWithEpoch() (T, uint) {
	b.valMtx.RLock()
	defer b.valMtx.RUnlock()
	return b.value, b.epoch
}

// GetChannelWithEpoch returns the current channel that notify on changes. In case it is not the current epoch that is passed in,
// a closed channel will be returned. This way you can detect updates between each wait when also using GetValueWithEpoch.
func (b *Broadcast[T]) GetChannelWithEpoch(epoch uint) (res chan struct{}) {
	b.notifyMtx.RLock()
	if b.notifyChannel != nil { // there is still a theoretical race condition before the first channel is created
		if b.epoch == epoch {
			res = b.notifyChannel
		} else {
			res = make(chan struct{})
			close(res)
		}
	}
	b.notifyMtx.RUnlock()
	if res == nil {
		b.notifyMtx.Lock()
		if b.notifyChannel == nil {
			b.notifyChannel = make(chan struct{})
		}
		res = b.notifyChannel
		b.notifyMtx.Unlock()
	}
	return res
}

// GetChannel returns the current channel that notify on changes
func (b *Broadcast[T]) GetChannel() chan struct{} {
	b.notifyMtx.RLock()
	res := b.notifyChannel
	b.notifyMtx.RUnlock()
	if res == nil {
		b.notifyMtx.Lock()
		if b.notifyChannel == nil {
			b.notifyChannel = make(chan struct{})
		}
		res = b.notifyChannel
		b.notifyMtx.Unlock()
	}
	return res
}

// SetValue sets a new value and informs listeners
func (b *Broadcast[T]) SetValue(newVal T) {
	b.notifyMtx.Lock()
	b.epoch++
	oldChan := b.notifyChannel
	b.notifyChannel = make(chan struct{})
	b.valMtx.Lock()
	b.value = newVal
	b.valMtx.Unlock()
	if oldChan != nil {
		close(oldChan)
	}
	b.notifyMtx.Unlock()
}
