package utils

import (
	"errors"
	"testing"
)

func TestErrors_Add(t *testing.T) {
	e1 := errors.New("err1")
	e2 := errors.New("err2")
	var err Errors
	err.Add(e1, e2, nil)
	if err.ErrOrNil() == nil {
		t.Error("should not be nil")
	}
	if len(err.errs) != 2 {
		t.Error("should have two errs, got", len(err.errs))
	}
	var err2 Errors
	err2.Add(&err)
	if len(err2.errs) != 2 {
		t.Error("should have two errs on err2, got", len(err2.errs))
	}
	err.Reset()
	if err.ErrOrNil() != nil {
		t.Error("should be nil")
	}
}
