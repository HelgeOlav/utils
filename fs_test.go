package utils

import (
	"context"
	"errors"
	"os"
	"testing"
)

func TestNewLocalFS(t *testing.T) {
	fs := NewLocalFS("./")
	ctx := context.TODO()
	// check a file that does not exist
	_, err := fs.Head(ctx, "doesnotexist")
	if err != nil && !errors.Is(err, os.ErrNotExist) {
		t.Error(err)
		return
	}
	// read some metadata
	m, err := fs.Head(ctx, "fs.go")
	if err != nil {
		t.Error(err)
		return
	}
	if m == nil {
		t.Error("no metadata from fs.go")
		return
	}
	if v, ok := m["meta"]; ok {
		if v != "data" {
			t.Error("wrong metadata value:", v)
			return
		}
	}
	// put a new object
	on := "DELETEME_SHOULD_NOT_EXIST"
	f, err := os.Open("fs.go")
	if err != nil {
		t.Error(err)
		return
	}
	err = fs.Put(ctx, on, m, f)
	_ = f.Close()
	if err != nil {
		t.Error(err)
		return
	}
	// write the object without metadata
	f, _ = os.Open("fs.go")
	err = fs.Put(ctx, on, nil, f)
	_ = f.Close()
	if err != nil {
		t.Error(err)
		return
	}
	// now get the metadata
	m, err = fs.Head(ctx, on)
	if err != nil {
		t.Error(err)
		return
	}
	if m != nil && len(m) > 0 {
		t.Error("metadata from fs.go")
		return
	}
	// delete the object
	err = fs.Delete(ctx, on)
	if err != nil {
		t.Error(err)
		return
	}
}
