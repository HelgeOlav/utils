package filewatcher

import (
	"context"
	"io/ioutil"
	"os"
	"sync/atomic"
	"testing"
	"time"
)

type simpleNotifier struct {
	count uint64
}

func (sn *simpleNotifier) NewCallback() func(string) {
	return func(fn string) {
		atomic.AddUint64(&sn.count, 1)
	}
}

func (sn *simpleNotifier) reset() {
	atomic.StoreUint64(&sn.count, 0)
}

func pause() {
	time.Sleep(300 * time.Millisecond)
}

func createFile(fn string, size int) error {
	message := make([]byte, size)
	return ioutil.WriteFile(fn, message, 0644)
}

func TestNewFileWatcher(t *testing.T) {
	workFile := os.TempDir() + "fwtest.txt"
	_ = os.Remove(workFile)

	ctx, cancel := context.WithCancel(context.Background())
	sn := simpleNotifier{}

	_, err := NewFileWatcher(ctx, workFile, sn.NewCallback(), time.Millisecond*50)
	if err != nil {
		t.Error(err.Error())
		return
	}
	// create a file
	err = createFile(workFile, 20)
	if err != nil {
		t.Error(err.Error())
		return
	}
	pause() // wait some time
	if sn.count != 1 {
		t.Error("FileWatcher failed to notify on created file")
	}
	// update file, short update, assume change is detected by size
	sn.reset()
	_ = createFile(workFile, 30)
	pause()
	if sn.count != 1 {
		t.Error("FileWatcher failed to notify on updated file (size)", sn)
	}
	// update file, long update, assume change is detected by timestamp
	time.Sleep(2 * time.Second)
	sn.reset()
	_ = createFile(workFile, 30)
	pause()
	if sn.count != 1 {
		t.Error("FileWatcher failed to notify on updated file (timestamp)", sn)
	}
	// cleanup
	cancel()
	pause()
}
