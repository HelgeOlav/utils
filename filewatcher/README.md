# FileWatcher

FileWatcher checks periodically if a file has changed and notifies if that is the case using a callback.

```go
package main

import (
	"fmt"
	"bitbucket.org/HelgeOlav/utils/filewatcher"
	"time"
	"context"
)

func mycallback(fn string) {
	fmt.Println(fn, "has changed")
}

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	fw := filewatcher.NewFileWatcher(ctx, "file-to-watch", mycallback, time.Second)
	// do some work
	time.Sleep(time.Minute)
	cancel() // stop filewatcher for this file
}
```