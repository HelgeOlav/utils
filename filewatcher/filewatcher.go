package filewatcher

import (
	"context"
	"errors"
	"os"
	"time"
)

// FileWatcher tracks file changes and issues a callback when a file has changed.
// The file is polled on given intervals to see if it has been changed and a callback is done if there is a change.
// If the file is deleted you will not get notified before a new file is in place.
type FileWatcher struct {
	fileName string          // name of the file we watch
	interval time.Duration   // interval between each check
	ctx      context.Context // used to end this watcher
	callback func(string)    // callback with name of file

	fileTime time.Time // modtime of file
	fileSize int64     // size of file
}

// NewFileWatcher creates a new filewatcher. The watcher is created if err is nil.

func NewFileWatcher(ctx context.Context, filename string, callback func(string), interval time.Duration) (*FileWatcher, error) {
	if len(filename) == 0 {
		return nil, errors.New("missing filename")
	}
	if interval <= 0 {
		return nil, errors.New("need positive interval")
	}
	if ctx == nil && interval > 0 {
		return nil, errors.New("no context passed in")
	}
	if callback == nil {
		return nil, errors.New("no callback passed in")
	}
	fw := &FileWatcher{
		fileName: filename,
		interval: interval,
		ctx:      ctx,
		callback: callback,
	}
	go fw.runner() // launch a background thread
	return fw, nil
}

// hasChanged returns true if the file has changed from the last time and also updates the internal structure at the same time
func (fw *FileWatcher) hasChanged() bool {
	fi, err := os.Stat(fw.fileName)
	// if we got an error - treat it like a file does not exist
	if err != nil {
		fw.fileSize = -1
		return false
	}
	var result bool
	// check if modtime has changed
	if !fi.ModTime().Equal(fw.fileTime) {
		result = true
		fw.fileTime = fi.ModTime()
	}
	// check for size
	if fi.Size() != fw.fileSize {
		result = true
		fw.fileSize = fi.Size()
	}
	return result
}

// runner runs in its own goroutine and checks the file on given intervals
func (fw *FileWatcher) runner() {
	fw.hasChanged() // run once to update the internal structure before we start
	ticker := time.NewTicker(fw.interval)
	defer ticker.Stop()
	for {
		select {
		case <-fw.ctx.Done():
			return
		case <-ticker.C:
			if fw.hasChanged() {
				fw.callback(fw.fileName)
			}
		}
	}
}
