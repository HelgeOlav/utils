package utils

import (
	"reflect"
	"testing"
)

func TestStringInString(t *testing.T) {
	myList := []string{"a", "b", "c"}
	myList2 := []string{}
	if !IsValueIn("a", &myList) {
		t.Error("Should have a in list")
	}
	if IsValueIn("a", &myList2) {
		t.Error("Should not have a in empty list")
	}
	if IsValueIn("a", nil) {
		t.Error("Should not have a in nil list")
	}
}

func TestRemoveDuplicate(t *testing.T) {
	type args[T comparable] struct {
		tSlice []T
	}
	type testCase[T comparable] struct {
		name string
		args args[T]
		want []T
	}
	tests := []testCase[string]{
		{name: "Test 1", args: args[string]{tSlice: []string{"a", "b", "a"}}, want: []string{"a", "b"}},
		{name: "Test 2", args: args[string]{tSlice: []string{"a", "b"}}, want: []string{"a", "b"}},
		{name: "Test 3", args: args[string]{tSlice: []string{}}, want: []string{}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RemoveDuplicate(tt.args.tSlice); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RemoveDuplicate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHighestValue(t *testing.T) {
	type args[T Ordered] struct {
		input []T
	}
	type testCase[T Ordered] struct {
		name       string
		args       args[T]
		wantResult T
	}
	tests := []testCase[int]{
		{name: "Test 1", args: args[int]{input: []int{2, 3, 6, 8, 0}}, wantResult: 8},
		{name: "Test 2", args: args[int]{input: []int{}}, wantResult: 0},
		{name: "Test 3", args: args[int]{input: []int{8}}, wantResult: 8},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := HighestValue(tt.args.input); gotResult != tt.wantResult {
				t.Errorf("HighestValue() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}

func TestLowestValue(t *testing.T) {
	type args[T Ordered] struct {
		input []T
	}
	type testCase[T Ordered] struct {
		name       string
		args       args[T]
		wantResult T
	}
	tests := []testCase[int]{
		{name: "Test 1", args: args[int]{input: []int{2, 3, 6, 8, 0}}, wantResult: 0},
		{name: "Test 2", args: args[int]{input: []int{}}, wantResult: 0},
		{name: "Test 3", args: args[int]{input: []int{8}}, wantResult: 8},
		{name: "Test 4", args: args[int]{input: []int{2, 3, -6, 8, 0}}, wantResult: -6},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if gotResult := LowestValue(tt.args.input); gotResult != tt.wantResult {
				t.Errorf("LowestValue() = %v, want %v", gotResult, tt.wantResult)
			}
		})
	}
}
