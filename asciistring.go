package utils

// OnlyAsciiText removes any non-ASCII (ASCII is: a-z, A-Z, 0-9) from input.
// If the input text is only special characters then an empty string is returned.
func OnlyAsciiText(input string) (output string) {
	for _, v := range input {
		if !((v < 'a' || v > 'z') && (v < 'A' || v > 'Z') && (v < '0' || v > '9')) {
			output = output + string(v)
		}
	}
	return
}
