//go:build go1.18

package utils

// MapDeepCopy copies the content of one map into another map
func MapDeepCopy[T comparable, V any](source map[T]V) (result map[T]V) {
	result = make(map[T]V)
	for x := range source {
		result[x] = source[x]
	}
	return
}
