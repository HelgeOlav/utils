package utils

import (
	"context"
	"fmt"
	"testing"
)

func TestGetRandomJoke(t *testing.T) {
	j := GetRandomJoke()
	if len(j.Value) == 0 {
		t.Error("failed go get a joke")
	} else {
		fmt.Println(j.Value)
	}
}

func TestGetLocalJoke(t *testing.T) {
	j := GetLocalJoke()
	if len(j.Value) == 0 {
		t.Error("failed go get a local joke")
	} else {
		fmt.Println(j.Value)
	}
	// see if we can get an out-of-bonds error
	for x := 0; x < 1000; x++ {
		_ = GetLocalJoke()
	}
}

func TestGetChuckNorrisJoke(t *testing.T) {
	_, err := GetChuckNorrisJoke()
	if err != nil {
		t.Error(err)
	}
}

func TestGetSv443Joke(t *testing.T) {
	jj, err := GetSv443Joke(context.Background())
	if err != nil {
		t.Error(err)
	}
	if jj.Error {
		t.Errorf("error from API; %v", jj)
		return
	}
	j := jj.ToJoke()
	if len(j.URL) == 0 {
		t.Error("missing URL")
	}
	if j.SourceProvider != JokeProviderJokeAPI {
		t.Error("wrong joke provider")
	}
	if len(j.Value) == 0 {
		t.Error("missing joke")
	}
}
