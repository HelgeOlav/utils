package utils

import (
	"log"
	"sync/atomic"
	"testing"
	"time"
)

const exitValue = -1

var bTestCounter uint64

func TestBroadcast(t *testing.T) {
	myVal := new(Broadcast[int])
	// launch something to count changes
	go func(myVal *Broadcast[int]) {
		log.Println("started notify")
		for {
			select {
			case <-myVal.GetChannel():
				log.Println("got notified on", myVal.GetValue())
				atomic.AddUint64(&bTestCounter, 1)
				if myVal.GetValue() == exitValue {
					log.Println("notify exit")
					return
				}
			}
		}
	}(myVal)
	time.Sleep(500 * time.Millisecond)
	// continue
	myVal.SetValue(4)
	time.Sleep(100 * time.Millisecond)
	myVal.SetValue(exitValue)
	time.Sleep(time.Second * 2)
	if myVal.GetValue() != exitValue {
		t.Error("wrong value returned")
	}
	if bTestCounter != 2 {
		t.Error("expected 2, got", bTestCounter)
	}
}

// TestBroadcast2 verifies that more than one recipient can receive the notification
func TestBroadcast2(t *testing.T) {
	myVal := new(Broadcast[int])
	myVal.SetValue(4)
	var counter uint64
	const workers = 5
	// create some listeners
	for x := 0; x < workers; x++ {
		go func() {
			timer := time.NewTimer(time.Second * 2)
			for {
				select {
				case <-timer.C:
					return
				case <-myVal.GetChannel():
					atomic.AddUint64(&counter, 1)
					timer.Stop()
					return
				}
			}
		}()
	}
	// publish some data
	time.Sleep(time.Millisecond * 100)
	myVal.SetValue(7)
	time.Sleep(time.Millisecond * 100)
	result := atomic.LoadUint64(&counter)
	if result != workers {
		t.Errorf("got %v results back, expected %v", result, workers)
	}
}

func TestBroadcast_GetChannelWithEpoch(t *testing.T) {
	br := Broadcast[Joke]{}
	br.SetValue(Joke{}) // set the epoch to 1
	_, ep := br.GetValueWithEpoch()
	// validate that the epoch is as expected
	if ep != 1 {
		t.Error("invalid epoch")
		return
	}
	// verify that the channel works
	ch := br.GetChannelWithEpoch(ep)
	select {
	case <-ch:
		t.Error("channel did not block on right epoch")
	default:
	}
	// try to get channel with invalid epoch
	ch = br.GetChannelWithEpoch(ep + 1)
	select {
	case <-ch:
	default:
		t.Error("channel blocked on wrong epoch")
	}
}
