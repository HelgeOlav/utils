package utils

import (
	"sort"
	"testing"
)

func TestModules(t *testing.T) {
	m := Modules{}
	m.AddModule(
		Module{
			Name:  "test 1",
			Order: 50,
		},
		Module{
			Name:  "test 2",
			Order: -100,
		})
	sort.Sort(m)
	curOrder := m.modules[0].Order
	for _, cm := range m.modules {
		if curOrder > cm.Order {
			t.Error("failed to sort list", m)
			return
		}
		curOrder = cm.Order
	}
}
