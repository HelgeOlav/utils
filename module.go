package utils

import (
	"flag"
	"fmt"
	"log"
	"sort"
	"time"
)

/*
	Module is a way for your program to register and launch parts of modules in an ordered fashion.
	Your modules register themselves and then the modules will be launched in order.
*/

// Module is a module in a list of Modules
type Module struct {
	Name         string        // name of module
	Runner       func()        // long-running task that is started in its own goroutine
	Description  string        // a brief help for the command line
	Hidden       bool          // true if this module should not be printed on command line and you need to know about it to launch it
	IncludeInAll bool          // if true, this module is launched with "all"
	Order        int           // startup order, defaults to 0, negative go last, positive go first
	StartupDelay time.Duration // if positive, will case a delay after starting up this module
}

// Modules are the container for modules that you launch your work from.
type Modules struct {
	modules []Module
}

func (T Modules) Len() int {
	return len(T.modules)
}

func (T Modules) Less(i, j int) bool {
	return T.modules[i].Order <= T.modules[j].Order
}

func (T Modules) Swap(i, j int) {
	T.modules[i], T.modules[j] = T.modules[j], T.modules[i]
}

// PrintHelp prints flag.PrintDefaults() and a list of all non-hidden modules in no particular order
func (m *Modules) PrintHelp() {
	flag.PrintDefaults()
	fmt.Println("\nModules:")
	for x := range m.modules {
		if !m.modules[x].Hidden {
			fmt.Printf("%10s: %s\n", m.modules[x].Name, m.modules[x].Description)
		}
	}
}

// StartModules starts all configured modules.
// As this list is ordered, the modules are started in the order defined by the list.
func (c *Modules) StartModules(module []string) {
	if len(module) == 1 && module[0] == "all" {
		c.StartAllModules()
	}
	for x := range module {
		for y := range c.modules {
			if module[x] == c.modules[y].Name {
				if c.modules[y].Runner != nil {
					go c.modules[y].Runner()
					if c.modules[y].StartupDelay > 0 {
						time.Sleep(c.modules[y].StartupDelay)
					}
				}
				break
			}
			log.Printf("module %s does not exist", module[x])
		}
	}
}

// StartAllModules will make a list of modules to start ordered by Order and start them
func (c *Modules) StartAllModules() {
	sort.Sort(c)
	var list []string
	for x := range c.modules {
		if c.modules[x].IncludeInAll {
			list = append(list, c.modules[x].Name)
		}
	}
	c.StartModules(list)
}

// AddModule adds a new module to the list of modules
func (c *Modules) AddModule(m ...Module) {
	c.modules = append(c.modules, m...)
}
