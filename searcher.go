package utils

import "strconv"

// Searcher is an interface that defines how to search for a match of data. All Searches has to be thread safe.
// The input can be any input supported by the implementation and the result is a bool to confirm
type Searcher interface {
	IsInList(interface{}) bool // search for a match, returns true if found
}

type StringArray []string // represents an array of strings

// IsInList implements Searcher on StringArray and returns true if input is in StringArray
func (s *StringArray) IsInList(input interface{}) bool {
	if s == nil {
		return false
	}
	var inputString string
	switch v := input.(type) {
	case string:
		inputString = v
	case []byte:
		inputString = string(v)
	case *string:
		inputString = *v
	case *[]byte:
		inputString = string(*v)
	case int:
		inputString = strconv.Itoa(v)
	case int64:
		inputString = strconv.FormatInt(v, 10)
	case uint64:
		inputString = strconv.FormatUint(v, 10)
	default:
		return false
	}
	for x := 0; x < len(*s); x++ {
		if (*s)[x] == inputString {
			return true
		}
	}
	return false
}
