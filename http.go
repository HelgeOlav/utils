package utils

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	"os"
)

const (
	HttpHeaderAuthorization = "Authorization"    // HttpHeaderAuthorization is the header that is used to authorize a client.
	HttpHeaderHost          = "Host"             // HttpHeaderHost is the host header. Remember that net/http sets this using a separate call.
	HttpHeaderContentType   = "Content-Type"     // HttpHeaderContentType is the header for content types
	HttpContentJson         = "application/json" // HttpContentJson is the content type for JSON
	HttpBearer              = "Bearer"           // bearer authorization
)

// SetHttpContentType sets content type of response to content
func SetHttpContentType(w *http.Response, content string) {
	w.Header.Set(HttpHeaderContentType, content)
}

// SetHttpContentTypeJson sets the content type to be JSON
func SetHttpContentTypeJson(w *http.Response) {
	SetHttpContentType(w, HttpContentJson)
}

// ContentTypeJson sets the content type to be JSON
func ContentTypeJson(w http.ResponseWriter) {
	w.Header().Set(HttpHeaderContentType, HttpContentJson)
}

// DownloadUrlWithContext will download an url, parse it and return it in a struct.
func DownloadUrlWithContext[T any](ctx context.Context, link string, headers map[string]string) (result T, err error) {
	var resp *http.Response
	result, resp, err = HttpRequest[T](ctx, headers, http.MethodGet, link, nil)
	if err != nil {
		return
	}
	if resp == nil {
		err = errors.New("no response from server")
	}
	if resp.StatusCode != http.StatusOK {
		err = errors.New("invalid http response " + resp.Status)
	}
	return
}

// DownloadUrl will download an url, parse it and return it in a struct.
// Deprecated: use DownloadUrlWithContext instead.
// This uses context.Background() as its context.
func DownloadUrl[T any](link string) (result T, err error) {
	return DownloadUrlWithContext[T](context.Background(), link, nil)
}

// DownloadLocalFile reads a file on the local file system and parses it into T
func DownloadLocalFile[T any](fn string) (result T, err error) {
	// read file
	bytes, err := os.ReadFile(fn)
	if err != nil {
		return
	}
	// parse content of file
	err = json.Unmarshal(bytes, &result)
	return
}

// HttpRequest is a generic HTTP call function that returns a JSON struct back to the application.
// The application should check both err and httpresponse.StatusCode to determine if the call was successful.
func HttpRequest[T any](ctx context.Context, headers map[string]string, method, url string, body io.Reader) (result T, httpresponse *http.Response, err error) {
	req, err := http.NewRequestWithContext(ctx, method, url, body)
	if err != nil {
		return
	}
	for x := range headers {
		req.Header.Set(x, headers[x])
	}
	httpresponse, err = http.DefaultClient.Do(req)
	if err != nil {
		return
	}
	myBody, err := io.ReadAll(httpresponse.Body)
	_ = httpresponse.Body.Close()
	if err == nil {
		err = json.Unmarshal(myBody, &result)
	}
	return
}
